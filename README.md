# HotFolderPrint

Simple and dirty script to print from a folder and move printed files to "Done" directory. Requires (awesome and fast) SumatraPDF application to be set as default pdf reader. Non pdf files are printed by sending their contents to printer.

Takes 3 params:
PrinterName - name of the printer, required (e.g. 'Microsoft Print to PDF');
WorkDir - printing directory (default '.');
FileExt - extention of the files (default '.pdf')
