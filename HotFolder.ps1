Param(
    [Parameter(Mandatory)]
    [String]$PrinterName,
    [String]$WorkDir = '.',
    [String]$FileExt = '.pdf'
)

If (-Not (Test-Path $WorkDir)) {
    Throw "The source directory $WorkDir does not exist, please specify an existing directory"
}

$files = (Get-ChildItem -path "$WorkDir\*" -include "*$FileExt" -exclude "$WorkDir\Done")
$date = Get-Date -Format "yyyyMMddHHmm"
$Printer = Get-wmiobject win32_printer -Filter ('name="' + $PrinterName + '"')
if ($Printer) {
    ForEach ($file in $files) {
        $FileName = $File.Name
        If ($File.Extension -eq ".pdf") {
            $argList = '-print-to "' + $PrinterName + '"'
            Start-Process -Wait $file.Name -argumentList $argList
        } else {
            Get-Content -path $File.FullName | Out-Printer -Name $PrinterName
        }
        If (-Not (Test-Path "$WorkDir\Done")) {
            New-Item -Path "$WorkDir\Done" -ItemType Directory
        }

        Move-Item -Path $File.FullName "$WorkDir\Done\$date$FileName"
    }
} else {
    Throw "Printer does not exist"
}
